<!DOCTYPE html>
<!-- In relation to the tabbed content on each page, certain IDs are reserved
---- for certain pages. 
---- #view1 to #view19 is reserved for the staff profiles page.
---- #view20 to #view29 is reserved for the therapy page
---- #view30 to #view39 is reserved for the rehabilitation page.
-->
<html>
    <head>
        <!-- setting the character encoding to UTF-8 -->
        <meta charset="UTF-8">
        
        <link rel="icon" type="image/x-icon" href="facicon.ico" />
        
        <!-- linking to the appropriate css sheet -->
        <link rel="stylesheet" type="text/css" href="css/all.css">
        <link rel="stylesheet" type="text/css" href="css/desktop.css">
        <link href="css/tabcontent.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" media="screen and (min-device-width: 451px) and (max-device-width: 799px)" href="css/tablet.css">
        
        <!-- JavaScript files -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/init.js"></script>
        <script src="js/contentChange.js"></script>
        <script src="js/tabcontent.js" type="text/javascript"></script>
        <script src="js/formHandler.js"></script>
        <script src="js/control.js"></script>

        <!-- jQuery Mobile redirect -->
        <script type="text/javascript">
            var mobileWidth = 768;
            if (screen.width <= mobileWidth) {
                window.location = "m/";
            }
        </script>
        
        <title>Ascent Rehabilitation</title>

        <meta name="description" content="Occupational therapy Canberra, our services include occupational 
        therapy and occupational rehabilitation. We will make sure you are able to get back into the workforce 
        when you are ready.">

    </head>
    
    <body>
        <img src="img/icons/close-button.svg" id="btnHideMenus" alt="X">
        <div id="wrapper">
            
            <div id="hidable">
                <?php include 'fragments/columns.php';?>
                <?php include 'fragments/services.php';?>
                <?php include 'fragments/support.php';?>
                <?php include 'fragments/assessment.php';?>
                <?php include 'fragments/about.php';?>
                <?php include 'fragments/contact.php';?>
            </div>
            <?php include 'fragments/nav.php';?>
        </div>
        <div class="linkBack">
            <a href="https://www.rocserve.com" target="_blank">POWERED BY ROCSERVE</a>
        </div>
    </body>
</html>