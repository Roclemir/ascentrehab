function enquirySubmit() {
    var url = '/sendEmail.php';
    var subject = escape( document.getElementById( 'emailSubject' ).value );
    var body = escape( document.getElementById( 'emailBody' ).value );
    
    $.ajax({
        url: url,
        type: 'post',
        datatype: 'jsonp',
        data: {'subject':subject, 'body':body },
        success: function(data) {
            $( '.enquirySuccess' ).fadeIn(1);
            alert(data);
        }
    });
}