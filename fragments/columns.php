<div id = "col-wrap" class="currentMainContent" >
    
    <div class = "col1 clickable services">
        <div class = "ch1" id="staff-header">
            <h2>OCCUPATIONAL</h2>
            <h2>THERAPY SERVICES</h2>
        </div>
        <img class="CLimage" src = "img/collumnPics/OccTherapyColPic_web.jpg" alt = "image">
        <div class="text-center">
        </div>
        <div class = "col-blurb">
            <div class="text-center">
                <p class="blurb-text">We offer a range of services to promote independence during your recovery.</p><br>
                
                <ul class="dotPoints">
                    <li>Return to Work Planning</li>
                    <li>RTW Program Implementation</li>
                    <li>Ergonomic Workstation Assessment</li>
                    <li>Equipment Prescription</li>
                    <li>Initial Needs Assessment</li>
                    <li>Worksite Assessment</li>
                    <li>Home Assessment</li>
                    <li>ADL Assessment</li>
                </ul>
            </div>
            
        </div>
    </div>
    
    <div class = "col2 clickable support">
        <div class = "ch2">
            <h2>SUPPORT AND</h2>
            <h2>COLLABORATION</h2>
        </div>
        <img class="CLimage" src = "img/collumnPics/supportAndCollabColPic_web.jpg" alt = "image">
        <div class="text-center">
            
        </div>
        <div class = "col-blurb">
                <div class="text-center">
                    <p class="blurb-text">Are you encountering difficulties in your recovery?</p><br>
                    
                    <ul class="dotPoints">
                        <li>Injury Management Advice</li>
                        <li>Medical Case Conferences</li>
                        <li>Treatment Case Conferences</li>
                        <li>Workplace Meetings</li>
                        <li>Mediation Services</li>
                    </ul>
                </div>
            </div>
        </div>
    
    <div class = "col3 clickable assessment">
        <div class = "ch3">
            <h2>MULTI-DISCIPLINARY</h2>
            <h2>ASSESSMENT</h2>
            <!--<h2>FACILITATION</h2>-->
        </div>
        <img class="CLimage" src = "img/collumnPics/multiDisciplanaryAsessColPic_web.jpg" alt = "image">
        <div class="text-center">

        </div>
        <div class = "col-blurb">
            <div class="text-center">
                <p class="blurb-text">The best outcomes are achieved through a collaborative approach.</p><br>
                
                <ul class="dotPoints">
                    <li>Physiotherapy Assessment</li>
                    <li>Vocational Assessment</li>
                    <li>Functional Capacity Evaluation</li>
                    <li>Pre-Employment Screening</li>
                    <li>Manual Handling Training</li>
                </ul>
            </div>
        </div>
    </div>
    
    <div class="col4 clickable about">
        
        <div class = "ch4">
            <h2>ABOUT ASCENT</h2>
            <h2>REHABILITATION</h2>
        </div>
        <img class="CLimage" src = "img/ascentTeam_web.jpg" alt="Photograph of the team at Ascent.">
        <div class="text-center">
        </div>
        <div class = "col-blurb">
            <div class="text-center">
                <p class="blurb-text">Come meet our team of Occupational Therapists.</p><br>
                
                <ul class="dotPoints">
                    <li>About Ascent</li>
                    <li>Jonathon</li>
                    <li>Renae</li>
                    <li>Emily</li>
                    <li>Lisa</li>
                    <li>Morgan</li>
                    <li>Isabella</li>
                </ul>
            </div>
        </div>
    </div>
</div> <!-- end col-wrap div -->