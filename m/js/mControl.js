/* global $ jQuery fadeIn fadeOut */
var expandedID;

$(function() {
  
  $( '.touchable' ).on( "tap", function() {
    openContent( $( this ) );
  })
  
  $( '.touchable' ).on( 'click', function() {
    openContent( $( this ) );
  });
  
});

// This function opens and closes the collapsable tabs
function openContent(touched) {
  
  if(this.expandedID == null) {
    $( '#mHeader' ).animate( {height: "15vh"}, 500 );
  }
  
  if(this.expandedID != null) {
    collapseContent(this.expandedID);
  }
  this.expandedID = expandContent( touched.attr( 'id' ) );
  
  function expandContent(contentID) {
    $( '#' + contentID ).next( '.mainContent' ).stop(true, true).animate( {height: "35vh"}, 500 );
    
    // returns the ID of the content that was expanded.
    return $('#' + contentID ).next( '.mainContent' ).attr( 'id' );
  }
  
  function collapseContent(expandedID) {
    $( '#' + expandedID ).animate( {height: "0px"}, 500);
    this.expandID = null;
  }
}