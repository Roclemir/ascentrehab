<div id="servicesMainContent" class="mainContent center">    

    <?php include 'servicesLinks.php'?>

    <h2 class="title" id="rtwProg">Return to Work Programs</h2>
    <p class="scrollTest">A return to work program is an overall structure that 
    is implemented by several key stakeholders, including the client, their employer, 
    nominated treating doctor, insurer and treatment providers. The program is documented 
    and outlines the key responsibilities of stakeholders and their involvement to assist the 
    client during their recovery. The program is goal oriented. 
    
    <h2 class="title" id="rtwPlan">Return to Work Planning</h2>
    <p class="scrollTest">A return to work plan is developed to assist the worker 
    at work, specifically providing details of the primary and secondary goals for the 
    client’s recovery. It outlines any medical restrictions, duties, hours and consideration
    of any special requirements. The process of developing a return to work plan generally 
    requires that a workplace assessment be conducted, collaboration occurs with medical and 
    treatment providers so that all restrictions are considered to ensure that the activities 
    performed are safe and sustainable.</p>

    <h2 class="title" id="ergonomic">Ergonomic Workstation Assessment</h2>
    <p class="scrollTest">Ascent provides ergonomic assessments to our existing and privately referred clients. 
    Ascent provides a comprehensive assessment of a person’s workstation, taking into account their daily 
    job requirements, any pre-existing injuries or current concerns. As part of the service, if requested, 
    Ascent is able to provide a comprehensive report for all parties records indicating both medical and 
    ergonomic advice.</p>

    <h2 class="title" id="equipment">Equipment Prescription</h2>
    <p class="scrollTest">Ascent can identify and source appropriate assistive equipment to maximise an 
    individual’s recovery.  Ascent will often meet with individuals in their home, community or work 
    environment to identify areas of concern; and to make recommendations on appropriate equipment in 
    order to make tasks easier relative to their stage of recovery.  Ascent has experience in providing a 
    range of equipment relevant to different settings.  These include but are not limited to workstation/desk 
    aids, bathroom aids, kitchen aids, long handled aids, and bedroom aids. Ascent can also refer clients 
    on to specialised services for wheelchair prescription and vehicle modifications.</p>
    
    <h2 class="title" id="initial">Initial Needs Assessment</h2>
    <p class="scrollTest">An assessment to identify the client’s concerns, needs, perceptions, 
    areas of interest and goals for their functional restoration or care. Ascent Rehabilitation 
    suggest an initial needs assessment occur for all new clients, to ensure that a holistic 
    approach is established in the early stages of a person’s injury and rehabilitation program 
    can be tailored according to a person’s individual needs.</p>
    
    <h2 class="title" id="home">Home Assessment</h2>
    <p class="scrollTest">Outside of the return to work sector, Ascent provides home assessment 
    services to assist a variety of individuals to become more independent in the home environment. 
    The aim of a home assessment is to identify the needs of each individual, and to provide assistance; 
    whether in the form of training, education, assistive equipment or environmental modification. Ascent 
    makes specific recommendations based on the individuals injuries, stage of recovery and social setting. 
    Considerations are specifically provided regarding access to the home both internally and externally; 
    with attention to an individual’s level of mobility and strength.</p>

    <h2 class="title" id="adl">Activities of Daily Living (ADL) Assessment</h2>
    <p class="scrollTest">An ADL Assessment is aimed to empower our clients to function independently 
    within their home and community environment.  An ADL Assessment is typically completed at the 
    clients home, and often involves information gathering in both a subjective and objective format. 
    Our clients are often asked to demonstrate various tasks including but not limited to; meal preparation, 
    cleaning/laundry, gardening, mobility and transfers, transport, shopping and personal hygiene. 
    The aim of this assessment is to identify areas of improvement, and to increase participation in 
    tasks through modifying the way it is executed or through recommending specific assistive equipment.</p>
    

    <h2 class="title" id="business">Worksite Assessments</h2>
    <p class="scrollTest">A worksite assessment is an important 
    part of the return to work process. This involves an assessment and analysis of the 
    person’s pre-injury duties, and establishes which of these specific duties are suitable 
    for a person to engage in throughout their recovery. Through early identification of suitable 
    duties, Ascent is able to best promote a functional, workplace based rehabilitation program 
    which keeps clients engaged in their usual daily tasks and routine.</p>
    
    <h2 class="title" id="vocational">Vocational Assessment</h2>
    <p class="scrollTest">Vocational assessments are a comprehensive tool to establish a person’s redeployment 
    options outside of their usual work role. The process of a vocational assessment typically requires a 
    variety of written and verbal assessments, aimed at establishing a person’s employability in other areas. 
    A vocational assessment can also identify areas for further retraining, making a client more suitable to 
    progress to new job roles.</p>
    
    <h2 class="title" id="physio">Physiotherapy Assessment</h2>
    <p class="scrollTest">For particular clients, Ascent can organise for an independent physiotherapy consultant to 
    provide one-off assessment of pre-existing or existing medical condition. This offers an impartial clinical 
    opinion and treatment recommendations for concerned parties. The independent physiotherapy assessments can be 
    especially beneficial to our clients who experience a plateau in their recovery and require a new approach to 
    their treatment program.</p>
    <br>
    <hr class="hrBottom">
    <a href="#rtwProg" class="backToTop">back to top</a>
</div>