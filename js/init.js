/* global $, timedFadeIn, mobileWidth fadeIn fadeOut*/
/* This file contains all functions that run when the page is loaded */

function start() {
        initFadeIn();
}

/* Initialises the fading in effect for the columns and the nav */
function initFadeIn() {
    // Time to wait in milliseconds before starting fade effect
    var startDelay = 2000; 
    
    // Amount of time the fade effect transition takes
    var fadeTime = 1000;
    
    // Delay between each column fading in
    var difference = 300;
    
    // CSS selectors of the columns in an array
    var cols = [ '.col1', '.col2', '.col3', '.col4' ];
    
    /* Fade in the nav */
    timedFadeIn( startDelay, fadeTime, 'nav' );
    
    /* Fade in all the columns */
    for (var i = 0; i < cols.length; ++i ) {
        timedFadeIn( startDelay * 2 + ( difference * i ), fadeTime, cols[i] );
    }
    
}