/* global $, DEBUG */

/*
 * This function hides all the site content
 * so the user may enjoy the background
 * picture
 */
function hideContent() {
    $( '#hidable' ).fadeOut();
}

/*
 * This function is the invert of the above
 */
function unHideContent() {
    $( '#hidable' ).fadeIn();
}

/* 
This function fades in elements that currently have their CSS display property set to none.
delayedStart is the time in milliseconds before fading in will commence
fadeTime is the amount of time in milliseconds that the fading effect will take to complete
selector is the CSS selector of the item you wish to fade in.
*/
function timedFadeIn( delayedStart, fadeTime, selector ) {
    setTimeout(
        function() {
            $( selector ).fadeIn(fadeTime);
        }, delayedStart 
    );
}

/* The inverse of the previous function */
function timedFadeOut( delayedStart, fadeTime, selector ) {
    setTimeout(
        function() {
            $( selector ).fadeOut( fadeTime );
        }, delayedStart
    );
}

/* Change the main content of the page depending on what was clicked */
function changeMainContent( changeTo ) {
    var delayedStart = 0;
    var fadeTime = 500;
    var currentID = '#' + $( '.currentMainContent' ).attr( 'id' );
    var mainContentClass = "currentMainContent";
    
    /* find out which item was clicked */
    if( changeTo.hasClass( 'home' ) ) {
        changer( '#col-wrap', '#navHome' );
    } else if ( changeTo.hasClass( 'about' ) ) {
        changer( '#aboutPage', '#navAbout' );
    } else if ( changeTo.hasClass( 'assessment' ) ) {
        changer( '#assessmentPage', '#navAssessment' );
    } else if ( changeTo.hasClass( 'services' ) ) {
        changer( '#servicesPage', '#navServices' );
    } else if ( changeTo.hasClass( 'contact' ) ) {
        changer( '#contactPage', '#navContact' );
    } else if (changeTo.hasClass( 'support' ) ) {
        changer( '#supportPage', '#navSupport' );
    } else if (changeTo.hasClass( 'X' ) ) {
        changer( '#col-wrap', '#navHome' );
    }
    
    /* Helper function to change main content */
    function changer( newID, linkID ) {
        
        /* out with the old and in with the new */
        timedFadeOut( delayedStart, fadeTime, currentID );
        timedFadeIn( delayedStart + fadeTime, fadeTime, newID );
        
        /* Adjust CSS classes */
        $( currentID ).toggleClass( mainContentClass );
        $( newID ).toggleClass( mainContentClass );
        
        /* Setup hyperlink highlighting */
        $( '.navSelected' ).toggleClass( 'navSelected' );
        $( linkID ).toggleClass( 'navSelected' );
    }
}