<div id="navWrapper">
    <nav>
        <h1 class="ascent title">ASCENT</h1>
        <h3 class="phrase title">REHABILITAITON DESIGNED FOR THE INDIVIDUAL</h3>
        <hr>
        
        <ul class="navUL">
          <li id="navHome" class="home navSelected clickable navMain">HOME</li>
          <li id="navServices" class="services navMain clickable">SERVICES</li>
          <li id="navSupport" class="support navMain clickable">SUPPORT</li>
          <li id="navAssessment" class="assessment navMain clickable">ASSESSMENT</li>
          <li id="navAbout" class="about navMain clickable">ABOUT US</li>
          <li id="navContact" class="contact navMain clickable">CONTACT</li>
        </ul>
    </nav>
</div>

