<div id="assessmentMainContent" class="mainContent center">
    
    <?php include 'assessmentLinks.php'?>
    
    <h2 class="title" id="physio2">Physiotherapy Assessment</h2>
    <p class = "scrollTest">For particular clients, Ascent can organise for an independent 
    physiotherapy consultant to provide one-off assessment of pre-existing or existing 
    medical condition. This offers an impartial clinical opinion and treatment 
    recommendations for concerned parties. The independent physiotherapy assessments 
    can be especially beneficial to our clients who experience a plateau in their 
    recovery and require a new approach to their treatment program.</p><br>
    
    <h2 class="title" id="vocational2">Vocational Assessment</h2>
    <p class = "scrollTest">Vocational assessments are a comprehensive tool to 
    establish a person’s redeployment options outside of their usual work role. 
    The process of a vocational assessment typically requires a variety of written 
    and verbal assessments, aimed at establishing a person’s employability in 
    other areas. A vocational assessment can also identify areas for further 
    retraining, making a client more suitable to progress to new job roles.</p><br>

    <h2 class="title" id="functional">Functional Capacity Evaluation</h2>
    <p class = "scrollTest">Ascent utilises WorkHab’s Functional Capacity Evaluation 
    program, which is a functional profiling tool aimed as assessing a client response 
    to a variety of tasks. This can be based on both work and daily activities. The 
    information is obtained by recording the clients individual response, using objective 
    physiological measures, clinical and biomechanical observations, as well as the clients 
    reported responses to tasks. The assessment is typically used for the purpose of assessing 
    a person’s capacity to return to their pre-injury occupations, or for the purpose of 
    assessing capacity for employment/redeployment.</p><br>
    
    <h2 class="title" id="pre">Pre-Employment Screening</h2>
    <p class="scrollTest">A pre-employment screen is a compressed form of a WorkHab Functional 
    Capacity Evaluation, which aims to review the specific physical demands of a specific 
    work role and provide objective measures of whether a client is able to demonstrate 
    such demands. A pre-employment screen is an effective way to review whether a potential 
    candidate is suitable for any proposed work role.</p>
    
    <h2 class="title" id="manual">Manual Handling Training</h2>
    <p class="scrollTest">Manual handling training is an imperative component of 
    safe work practices, and is the most effective means of ensuring staff safety 
    in environments which require heavy lifting. Manual handling training can be 
    provided in a range of formats specific to any work environment, in a delivery 
    method of your choice (presentation, demonstration). Manual handling training 
    has been proven to reduce the risk of workplace injuries, providing staff with 
    essential knowledge of muscular activation and the importance of appropriate 
    technique.</p>
    <br>
    <hr>
    <a href="#physio2" class="backToTop">back to top</a>
</div>