<div id="supportMainContent" class="mainContent center">

    <?php include 'supportLinks.php'?>

    <h2 class="title" id="injury">Injury Management Advice</h2>
    <p class = "scrollTest">The team at Ascent pride themselves on maintaining an up to date 
    medical knowledge, and all hold relevant health professional training in the form of a 
    Bachelors or Master’s degree (Occupational Therapy). Ascent provides injury management 
    advice to assist our clients following an injury. With our understanding of the 
    musculoskeletal system, we are able to guide our clients during their recovery through 
    medically justified treatment programs in collaboration with their treating GP.  
    We provide education on postural, safe movement and activity to support a person to 
    have a successful recovery and allow participation at work, at home and in the community. 
    Ascent has a core focus on clinical reasoning, striving to ensure that all recommendations 
    made are in consideration of the most up to date medical information available.  </p><br>
    
    <h2 class="title" id="medical">Medical Case Conferences</h2>
    <p class = "scrollTest">Effective rehabilitation requires good communication between all parties. 
    As a rehabilitation consultant, our role is to ensure that the gradual return to work 
    program remains adherent to the limitations specified by the client’s general practitioner. 
    Medical case conferences assist in facilitating discussion between the client, rehabilitation 
    consultant and the GP to ensure that all aspects of the return to work plan are within the 
    client’s medical limitations. It ensures that all tasks that may cause aggravation to the 
    injured area are avoided; promoting a more timely recovery. </p><br>

    <h2 class="title" id="treatment">Treatment Case Conferences</h2>
    <p class = "scrollTest">As with medical case conferences, treatment case conferences are an 
    imperative component of the gradual return to work program, to ensure that the treating 
    parties maintain a goal oriented program which best supports that rehabilitation goals. 
    Ascent believes that early collaboration with all treatment providers is the most effective 
    way to ensure consistency and eliminate the potential for conflicting ideologies in the 
    early stages of medical recovery. Treatment case conferences can be conducted face-to-face, 
    or over the phone, with the client always involved in discussions about their ongoing 
    rehabilitation.  </p><br>
    
    <h2 class="title" id="workplace">Workplace Meetings</h2>
    <p class="scrollTest">Workplace meetings are often held to ensure that all key parties 
    are working together to achieve a mutual goal. A workplace meeting provides the 
    opportunity to devise a plan or strategy for coordinating all aspects of a client’s 
    rehabilitation (which as we know, should always include a return to work in some capacity).</p>
    
    <h2 class="title" id="mediation">Mediation Services</h2>
    <p class="scrollTest">Workplace mediation is a confidential process where Ascent 
    (an impartial and independent third person) facilitates communication between two or 
    more people in dispute. Ascent’s role as a workplace mediator is to provide a structured 
    process that assists with identifying issues, developing options and reaching an agreement 
    that satisfies the needs of both an employer and their employee. The goal of workplace 
    mediation is to empower people to take responsibility for the issues and the resolution. 
    People are encouraged to view the dispute within the broader context and from multiple 
    perspectives.</p>
    <br>
    <hr>
    <a href="#injury" class="backToTop">back to top</a>
</div>