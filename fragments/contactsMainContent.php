<div id="contactsMainContent" class="mainContent">
    
    <div class="tabcontents">
        <div class="contactDetails headerSpacing">
            <div class="notMobile" style="height:15vh"></div>
            <p class="contentP bold contentHeader">Contact Details</p>
            <p class="contentP">Ascent Rehabilitation Pty Ltd<br>
                Level 1, 8 Petrie Plaza<br>
                Canberra ACT 2601</p>
            <p class="contentP">Phone: (02) 6156 4109</p>
            <p class="contentP"><?php echo '<p class="contentP">Email: <a href="mailto:admin@ascentrehab.com?Subject=Enquiry" target="_top">admin@ascentrehab.com</a>'; ?></p>
        </div>
<iframe class="mapPic" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2303.0762093550698!2d149.1299670271478!3d-35.2800916885991!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6b164d6895160e43%3A0xd34a5151346bb7fa!2s8+Petrie+Plaza%2C+Canberra+ACT+2601!5e0!3m2!1sen!2sau!4v1485493404441" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>    </div>
</div>