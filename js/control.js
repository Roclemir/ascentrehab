/* global jQuery, $, start, changeMainContent hideContent unHideContent*/

/* Initialise the page */
$(function() {
    start();
    /* Setup change on click */
    $( '.clickable' ).on( 'click', function() {
        changeMainContent( $( this ) );
    });
    
    $( '#btnHideMenus' ).on( 'mouseover', function() {
        hideContent();
    });
    
    $( '#btnHideMenus' ).on( 'mouseout', function() {
       unHideContent(); 
    });
    
    // Handles form submission and validation
    // $("#email").validate();
});

