<!-- see comment at the top of index.php reguarding the #view IDs -->
<div id="aboutMainContent" class="mainContent">
    <ul class="tabs notMobile">
        <li><a href="#view0">Ascent Rehab</a></li>
        <li><a href="#view1">Jonathan Ford</a></li>
        <li><a href="#view2">Renae Pickering</a></li>
        <li><a href="#view3">Lisa Flynn</a></li>
        <li><a href="#view4">Emily Mayne</a></li>
        <!--<li><a href="#view5">Isabella Toole</a></li>-->
        <li><a href="#view6">Morgan Robertson</a></li>
    </ul>
    <div class="tabcontents-staff">
        
        <div id="view0">
            
            <div class="tabcontents-staffInfo">
                <h3 class="staffName">About Ascent Rehabilitation</h3>
                
                <p class="break">Our Mission: To inspire, empower and create solutions for people affected by injury and illness, or disability and disease. </p>
                
                <p>Ascent Rehabilitation was founded by Jonathan Ford in April,
                2010 to address the need for a consistent and reliable 
                occupational therapy service in the ACT. Jonathan believes 
                that occupational therapy should be supportive, collaborative, 
                evidence-based and ethical.</p><br>

                <p>Ascent Rehabilitation has grown organically; initially 
                employing an occupational therapist and an office manager.
                More recently, Ascent has grown to employ more than five
                therapists and now provides physiotherapy and psychological 
                consultancy.</p><br>

                <p>Ascent's therapists pride themselves on applying a problem 
                solving approach to assist each client. Ascent aims to empower 
                clients to identify and apply their own management strategies to 
                assist them to adapt to their environment, minimising their risk
                of exacerbation or (re)injury.</p><br>

                <p>Clients are encouraged to maintain activity in their chosen
                occupations with the assistance of modification and adaptation.
                Ideally, this will assist clients to build resilience and achieve
                their goals of a return to maximum level of function and 
                independence.</p><br>
            </div>
            
            <div class="tabcontents-team">
                <div class="picRight">
                    <img src="/img/ascentTeam_web.jpg" class="teamPic">
                </div>
            </div>
        </div>
        
        <div id="view1">
            <!--<div class="staff-wrapper">-->
            <!--</div>-->
        
            <div class="tabcontents-staffInfo">
                <h3 class="staffName">Jonathon Ford</h3>
                <div class="badgeStrip break">
                    <a href="http://www.apsoc.org.au/" target="_blank" ><img src="http://www.apsoc.org.au/img/logo.jpg" class="badges"></a>
                    <a href="https://www.ahpra.gov.au/" target="_blank" ><img src="/img/ahpra.png" class="badges"></a>
                </div>
                <!--<img src = "img/busman.jpg" class = "placeholder">-->
                <!--<p>List of affiliation badges goes here</p>-->
                
                <p>Position: Director</p>
                <p class="break">Qualifications: Occupational Therapist/ Ergonomist</p>
                <p class="break">Additional Training: Suicide Prevention, Motivational Interviewing, Cert IV in Mediation, Yearly attendance at the Australian Pain Conference</p>
                <p>Individual professional club or association affiliations: Australian Pain Society - Affiliate Member, AHPRA Accredited, Occupational Therapy 
                Association</p>
                
            </div>
            
            <div class="tabcontents-team">
                <div class="picRight">
                    <img src="/img/ascentTeam_web.jpg" class="teamPic notMobile">
                </div>
            </div>
            
            <div class="tabcontents-blurb">
                    <p>Jon has been practicing as an Occupational Therapist in the ACT for the majority of his career. Jonathan began his career working for a local Occupational 
                    Rehabilitation provider, before deciding to open his own business in 2010. Since this time, Jonathan has allowed his business to grow organically, and is 
                    proud to be able to offer employment to his ever growing group of staff. Jon is a father of two. When he isn’t working from the couch, he is breaking up 
                    fights and dealing with his children’s tantrums. When the kids are in bed, he is usually found writing reports, or talking to his wife (the real boss of 
                    Ascent Rehabilitation). Jon then looks forward to a glass of red wine and a few moments of downtime.</p>
            </div>
        </div>
        
        <div id="view2">
            <div class="tabcontents-staffInfo">
                <h3 class="staffName">Renae Pickering</h3>
                <div class="badgeStrip break">
                    <a href="http://www.apsoc.org.au/" target="_blank" ><img src="http://www.apsoc.org.au/img/logo.jpg" class="badges"></a>
                    <a href="https://www.ahpra.gov.au/" target="_blank" ><img src="/img/ahpra.png" class="badges"></a>
                    <a href="http://www.workhab.com/" target="_blank"><img src="/img/workhab.png" class="badges workhab"></a>
                </div>
                
                <p>Position: Senior Occupational Therapist</p>
                <p class="break">Qualifications: Bachelor of Occupational Therapy (Charles Sturt University)</p>
                <p class="break">Additional Training: Accredited WorkHab Functional Capacity Evaluator (2015) with additional training in First Aid (2010), Mental Health First Aid (2010), 
                Suicide Intervention Skills Training (2013) and Motivational Interviewing (2014). 
                Yearly attendee of the Australian Pain Societies annual scientific meetings (2015, 2016, 2017).</p>
                <p class="break">Individual professional club or association affiliations: 
                Australian Health Practitioners Regulation Agency Accredited, WorkHab Accredited, Australian Pain Society Affiliated.</p>
                
            </div>
            
            <div class="tabcontents-team">
                <div class="picRight">
                    <img src="/img/ascentTeam_web.jpg" class="teamPic notMobile">
                </div>
            </div>
            
            <div class="tabcontents-blurb">
                <p>Renae is a senior member of the Ascent Rehabilitation team. Growing up in Geelong, Victoria; Renae has always been an avid 
                supporter of the Geelong football club (carn’ the cats). She then moved to Albury to 
                complete her Bachelor of Occupational Therapy at Charles Sturt University before moving to Canberra to 
                pursue a career as an occupational therapist. Renae has a passion for all things active with a particular interest in competitive netball. 
                On the weekends, she likes to spend time with her golden retriever, Ernie, and unwind over a nice bottle of sauvignon blanc.</p>
            </div>
        </div>
    
        <div id="view3">
            <div class="tabcontents-staffInfo">
                <h3 class="staffName">Lisa Flynn</h3>
                <p>Position: Office Manager</p>
                <p class="break">Qualifications: Currently undertaking a diploma of Human Resources.</p>
                
                <p>Lisa is a foundation member of the Ascent Rehabilitation team, who joined Director, 
                Jonathan Ford in 2012. Lisa has many years of management experience, particularly in the 
                occupational rehabilitation setting. Lisa holds a wealth of knowledge regarding occupational 
                rehabilitation and is a mentor to all of the Ascent staff. When Lisa isn’t busy running the 
                office, she enjoys spending time with her toddler and husband. Lisa loves relaxing at her 
                local coffee shop, but is still seeking the perfect latte. Suggestions welcome. </p>
            </div>
            
            <div class="tabcontents-team">
                <div class="picRight">
                    <img src="/img/ascentTeam_web.jpg" class="teamPic notMobile">
                </div>
            </div>
        </div>
        
        <div id="view4">
            <div class="tabcontents-staffInfo">
                <h3 class="staffName">Emily Mayne</h3>
                <div class="badgeStrip break">
                    <a href="http://www.apsoc.org.au/" target="_blank" ><img src="http://www.apsoc.org.au/img/logo.jpg" class="badges"></a>
                    <a href="https://www.ahpra.gov.au/" target="_blank" ><img src="/img/ahpra.png" class="badges"></a>
                    <a href="http://www.mda.org.au/" target="_blank" ><img src="/img/mda.png" class="badges"></a>
                </div>
                
                <p>Position: Occupational Therapist</p>
                <p class="break">Qualifications: Bachelor of Occupational Therapy</p>
                <p class="break">Additional Training: Motivational Interviewing (2015), Apply First Aid (2015), Suicide Intervention Skills Training (2016), attendee of the 
                Australia Pain Societies Annual Scientific Meeting (2016).</p>
                <p class="break">Individual professional Australia Health Practitioners Regulation Agency Accredited, Australian Pain Society Affiliated, 
                Muscular Dystrophy Association Affiliated.</p>
            </div>
            <div class="tabcontents-team">
                <div class="picRight">
                    <img src="/img/ascentTeam_web.jpg" class="teamPic notMobile">
                </div>
            </div>
            
            <div class="tabcontents-blurb">
                <p>Emily graduated with a Bachelor of Occupational Therapy at Deakin University in Geelong, moving to Canberra shortly after to 
                pursue a career in Occupational Rehabilitation. Emily enjoys the easy lifestyle Canberra has to offer, 
                however often escapes the long weekends and holidays to spend time with family and friends in Melbourne. 
                Emily chose this particular path as she has a passion for helping others, and seeing people reach their 
                full potential. Emily has always enjoyed travelling, and lived in Canada for a short period of time. In 
                her spare time, Emily enjoys relaxing with a good book and watching the latest Netflix drama.</p>
            </div>
        </div>
        
        <!--<div id="view5">-->
        <!--    <div class="tabcontents-staffInfo">-->
        <!--        <h3 class="staffName">Isabella Toole</h3>-->
        <!--        <div class="badgeStrip break">-->
        <!--            <a href="http://www.apsoc.org.au/" target="_blank" ><img src="http://www.apsoc.org.au/img/logo.jpg" class="badges"></a>-->
        <!--            <a href="https://www.ahpra.gov.au/" target="_blank" ><img src="/img/ahpra.png" class="badges"></a>-->
        <!--        </div>-->
        <!--        <p>Position: Occupational Therapist</p>-->
        <!--        <p class="break">Qualifications: Bachelor of Occupational Therapy</p>-->
        <!--        <p class="break">Additional Training: Motivational Interviewing (2016), Apply First Aid (2015), attendee of the Australia Pain Societies Annual Scientific Meeting (2016).</p>-->
        <!--        <p class="break">Individual professional club or association affiliations: -->
        <!--        Australia Health Practitioners Regulation Agency Accredited, Australian Pain Society Affiliated, Occupational Therapy Association Affiliated.</p>-->
        <!--    </div>-->
            
        <!--    <div class="tabcontents-team">-->
        <!--        <div class="picRight">-->
        <!--            <img src="/img/ascentTeam_web.jpg" class="teamPic notMobile">-->
        <!--        </div>-->
        <!--    </div>-->
            
        <!--    <div class="tabcontents-blurb">-->
        <!--        <p>Isabella graduated with a Masters of Occupational Therapy at the University of Canberra. Following completion of an -->
        <!--        undergraduate degree in marketing, Isabella chose to pursue a Master’s degree and career in -->
        <!--        Occupational Therapy because of its emphasis on maintaining a person’s independence and -->
        <!--        participation in activities; despite having an illness or injury. As a Canberra local, -->
        <!--        Isabella enjoys spending time with her large family and catching up with friends as often as she can. -->
        <!--        Isabella has a passion for Yoga, Pilates and all things relaxing. </p>-->
        <!--    </div>-->
        <!--</div>-->
        
        
        <div id="view6">
            <div class="tabcontents-staffInfo">
                <h3 class="staffName">Morgan Robertson</h3>
                
                <div class="badgeStrip break">
                    <a href="https://www.ahpra.gov.au/" target="_blank" ><img src="/img/ahpra.png" class="badges"></a>
                </div>
                
                <p>Position: Occupational Therapist</p>
                <p class="break">Qualifications: Bachelor of Applied Science (University of Canberra), Masters of Occupational Therapy (University of Canberra)</p>
                <p class="break">Attendance at the Australasian Occupational Science Symposium in Auckland (2016), First Aid Training (2014)</p>
                <p class="break">Individual professional club or association affiliations: 
                Australian Health Practitioners Regulation Agency Accredited</p>
            </div>
            
            <div class="tabcontents-team">
                <div class="picRight">
                    <img src="/img/ascentTeam_web.jpg" class="teamPic notMobile">
                </div>
            </div>
            
            <div class="tabcontents-blurb">
                <p>Morgan is the newest member of the Ascent Rehabilitation team, and brings with her a wealth of experience across acute 
                rehabilitation, paediatrics and mental health. Whilst Morgan enjoyed the core focus of traditional 
                occupational therapy, she has always had an interest in occupational rehabilitation and the holistic 
                approach it promotes. Morgan enjoys working with clients from the beginning of their journeys, all the 
                way through to their return to normality. She is always planning for her next overseas adventure, which 
                allows her to sample coffee and wine from all over the world. Morgan looks forward to spending time with 
                her partner and family on the weekends in her home town of Goulburn. And yes, she drives 90 minutes too 
                and from work each day!</p>
            </div>
        </div>
    </div>
</div>