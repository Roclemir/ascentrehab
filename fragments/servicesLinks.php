<ul class="dotPointsMain notMobile">
    <a href="#rtwProg">Return to Work Program</a>
    <a href="#rtwPlan">Return to Work Planning</a>
    <a href="#ergonomic">Ergonomic Workstation Assessment</a>
    <a href="#equipment">Equipment Prescription</a>
    <a href="#initial">Initial Needs Assessment</a>
    <a href="#home">Home Assessment</a>
    <a href="#adl">ADL Assessment</a>
    <a href="#business">Worksite Assessment</a>
    <a href="#vocational">Vocational Assessment</a>
    <a href="#physio">Physiotherapy Assessment</a>
</ul><br><br><br>