
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta author="Ryan Couper; Nicolas Slater">
        <meta description="Ascent Rehabilitation for mobile; occupational; therapy; rehabilitation">
        
        <!-- jQuery aned jQuery mobile CDNs -->
        <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
        
        <!-- local stylesheets -->
        <link rel="stylesheet" href="../css/all.css" />
        <link rel="stylesheet" href="css/mobile.css" />
        
        <!-- local JS files -->
        <script src="js/mControl.js"></script>
        
    </head>
    <body>
        <div id="mWrapper">
            <div class="mHeader" id="mHeader">
                <h1 class="title mAscent">ASCENT</h1>
                <h2 class="title mPhrase">REHABILITAITON DESIGNED FOR THE INDIVIDUAL</h2>
            </div>
                <div id="services-cell" class="titleCell touchable">
                    <img src="img/icons/plus-white.png" alt="" class="icnPlus">
                    <h1>SERVICES</h1>
                </div>
                <?php include '../fragments/servicesMainContent.php';?>
            
                <div id="support-cell" class="titleCell touchable">
                    <img src="img/icons/plus-white.png" alt="" class="icnPlus">
                    <h1>SUPPORT</h1>
                </div>
                <?php include '../fragments/supportMainContent.php'; ?>
            
                <div id="assessment-cell" class="titleCell touchable">
                    <img src="img/icons/plus-white.png" alt="" class="icnPlus">
                    <h1>ASSESSMENTS</h1>
                </div>
                <?php include '../fragments/assessmentMainContent.php'; ?>
            
                <div id="about-cell" class="titleCell touchable">
                    <img src="img/icons/plus-white.png" alt="" class="icnPlus">
                    <h1>ABOUT</h1>
                </div>
                <?php include '../fragments/aboutMainContent.php'; ?>
            
                <div id="contacts-cell" class="titleCell touchable">
                    <img src="img/icons/plus-white.png" alt="" class="icnPlus">
                    <h1>CONTACTS</h1>
                </div>
                <?php include '../fragments/contactsMainContent.php'; ?>
        </div>
    </body>    
</html>